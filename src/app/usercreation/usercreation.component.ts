import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AddUserService } from '../add-user.service';

@Component({
  selector: 'app-usercreation',
  templateUrl: './usercreation.component.html',
  styleUrls: ['./usercreation.component.css']
})
export class UsercreationComponent implements OnInit {
  userForm:any;data:any; submitted = false;

  constructor(private addiserS:AddUserService,private fb:FormBuilder) { }

  ngOnInit(): void {
    this.userForm=this.fb.group({
      fName:['',Validators.required],
      LName:['',Validators.required],
      Emial:['',Validators.required],
      MNo:['',Validators.required],
      Age:['',Validators.required],
      State:['',Validators.required],
      contry:['',Validators.required],
      tags:['',Validators.required]
    })
  }
  get f(): { [key: string]: AbstractControl } {
    return this.userForm.controls;
  }
  insert()
  { this.submitted = true;

    
    if (this.userForm.invalid) {
      return;
    }

    let ab={
      FName:this.userForm.value.fName,
      LName:this.userForm.value.LName,
      Email:this.userForm.value.Emial,
      MNo:this.userForm.value.MNo,
      Age:this.userForm.value.Age,
      State:this.userForm.value.State,
      Contry:this.userForm.value.Contry,
      tags:this.userForm.value.tags
    }
    this.addiserS.adduser(ab).subscribe(res=>{
        this.data=res;
    })
  }

}

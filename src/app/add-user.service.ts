import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddUserService {

  constructor(private _http: HttpClient) { }
  adduser(data: any) {

    return this._http.post("http://localhost:3000/posts", data)
  }
  getdata() {

    return this._http.get("http://localhost:3000/posts ")
  }
  getdatabyID(id: any) {

    return this._http.get("http://localhost:3000/posts?id=" + id)
  }
  Update(id: any, data: any) {

    return this._http.put<any>("http://localhost:3000/posts/" + id, data)
  }
  Delete(id: any) {

    return this._http.delete("http://localhost:3000/posts/" + id)
  }
  upload(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', "http://localhost:3000/upload", formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this._http.request(req);
  }

  getFiles(): Observable<any> {
    return this._http.get("http://localhost:3000/files");
  }
}

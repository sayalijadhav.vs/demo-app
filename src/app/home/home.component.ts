import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { AddUserService } from '../add-user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userForm: any;
  imgsrc: any;
   addbtn: boolean = false;
  updatebtn: boolean = false;
  constructor(private fb: FormBuilder, private addiserS: AddUserService, public _d: DomSanitizer) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      fName: ['', Validators.required],
      LName: ['', Validators.required],
      Emial: ['', Validators.required],
      MobileNO: ['', Validators.required],
      Age: ['', Validators.required],
      State: ['State', Validators.required],
      contry: ['Country', Validators.required],
      Address: ['', Validators.required]
    })
    this.getdata();
    
  }
  get f(): { [key: string]: AbstractControl } {
    return this.userForm.controls;
  }

  fileChange(e: any) {
    const file = e.srcElement.files[0];
    this.imgsrc = window.URL.createObjectURL(file);

  }

  submitted: any; data: any = [];

  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      alert("Enter Only Number")
      return false;
    }
    return true;

  }
  allowOnlyLetters(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;

    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
      return true;
    else {
      alert("Please enter only alphabets");
      return false;
    }
  }
  getdata() {
    this.addiserS.getdata().subscribe(res => {
      this.data = res;
    })

  }
  editdata: any; id: any;
  edit(id: any) {
    this.updatebtn=true;
    this.addbtn=false
    this.id = id;
    this.addiserS.getdatabyID(id).subscribe(res => {
      this.editdata = res;
      this.userForm.controls['fName'].setValue(this.editdata[0].FName);
      this.userForm.controls['LName'].setValue(this.editdata[0].LName);

      this.userForm.controls['MobileNO'].setValue(this.editdata[0].MobileNO);
      this.userForm.controls['Age'].setValue(this.editdata[0].Age);
      this.userForm.controls['contry'].setValue(this.editdata[0].Contry);
      this.userForm.controls['Address'].setValue(this.editdata[0].Address);
      this.userForm.controls['Emial'].setValue(this.editdata[0].Email);
      this.userForm.controls['State'].setValue(this.editdata[0].State);
    })
    let element: HTMLElement = document.getElementById('Register') as HTMLElement;
    element.click();
    this.editdata = [];
  }
  Update() {
    this.updatebtn=true;
    this.addbtn=false
    this.submitted = true;
    if (this.userForm.invalid) {
      return;
    }
    let ab = {
      FName: this.userForm.value.fName,
      LName: this.userForm.value.LName,
      Email: this.userForm.value.Emial,
      MobileNO: this.userForm.value.MobileNO,
      Age: this.userForm.value.Age,
      State: this.userForm.value.State,
      Contry: this.userForm.value.contry,
      tags: this.userForm.value.tags,
      Address: this.userForm.value.Address,
    }
    this.addiserS.Update(this.id, ab).subscribe(res => {
      this.data = res;
      alert("Update Successfully");

      this.getdata();
      this.data = [];
      window.location.reload();
      let element: HTMLElement = document.getElementById('btnclose') as HTMLElement;
      element.click();
    })

  }
  Delete(id: any) {
    this.addiserS.Delete(id).subscribe(res => {
      this.data = res;
      alert("Delete Successfully");
      this.getdata();
      this.data = [];
    })
  }
  add() {
    this.updatebtn=false;
    this.addbtn=true;
    // this.data = null;
    // this.data = [] = [];
    let element: HTMLElement = document.getElementById('Register') as HTMLElement;
    element.click();
  }
  insert() {
    this.submitted = true;


    if (this.userForm.invalid) {
      return;
    }
    // this.userForm.markAllAsTouched();
    // if (this.userForm.valid) {
    //   console.log(this.userForm.value);
    // }
    let ab = {
      FName: this.userForm.value.fName,
      LName: this.userForm.value.LName,
      Email: this.userForm.value.Emial,
      MobileNO: this.userForm.value.MobileNO,
      Age: this.userForm.value.Age,
      State: this.userForm.value.State,
      Contry: this.userForm.value.contry,
      tags: this.userForm.value.tags,
      Address: this.userForm.value.Address,
    }
    this.addiserS.adduser(ab).subscribe(res => {
      this.data = res;
      alert("User Added Successfully");
      this.userForm.rest();
      window.location.reload();
    })
    this.getdata();
    window.location.reload();
    let element: HTMLElement = document.getElementById('btnclose') as HTMLElement;
    element.click();
  }

}
